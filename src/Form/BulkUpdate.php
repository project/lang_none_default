<?php

namespace Drupal\lang_none_default\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BulkUpdate.
 */
class BulkUpdate extends FormBase {

  /**
   * Nids to update.
   *
   * @var array
   */
  public $tablesToUpdate;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bulk_update';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->tablesToUpdate = $this->getTableNames();

    if (count($this->tablesToUpdate) === 0) {
      $form['tablecount'] = [
        '#markup' => $this->t("Could not find any tables."),
      ];

      return $form;
    }

    $form['tablecount'] = [
      '#prefix' => '<p>',
      '#suffix' => '</p>',
      '#markup' => $this->t("<p>Your site has @count database tables.</p><p>This bulk updater loops through each table (10 per batch) in the database.</p><p>For each table that contains a <em>langcode</em> column it will change <strong>und</strong> langcode values to the site's default langcode (<strong>@lang</strong> in your case).</p><p><em>NOTICE</em><br/>If you have <strong>und</strong> <em>and</em> <strong>@lang</strong> url_alias table values (for the same source) you may want to run these first (to delete the <strong>und</strong> dupes):</p><p><strong>drush ev \"lang_none_default_remove_und_dupe_aliases();\"</strong> (report only, aka Dry Run)<br/><strong>drush ev \"lang_none_default_remove_und_dupe_aliases(FALSE);\"</strong> (actually delete dupes)</p><p>You may also want to check the <em>redirect</em> table and deal with any duplicate und/@lang aliases there.</p>",
        [
          '@count' => count($this->tablesToUpdate),
          '@lang' => \Drupal::languageManager()->getDefaultLanguage()->getId(),
        ]),
    ];

    $form['warning'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('I understand there is <strong>no confirmation or undo</strong> after I click the button below.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update Database Tables'),
    ];

    return $form;
  }

  /**
   * Returns an array of all table names in the database.
   *
   * @return array
   *   Array of tables to update.
   */
  public function getTableNames() {
    $db = Database::getConnection();
    $query = $db->query('SHOW TABLES');
    return $query->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('warning'))) {
      $form_state->setErrorByName('warning',
        t('You must confirm that you understand the risks.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operations = [];
    foreach (array_chunk($this->tablesToUpdate, 10) as $smaller_batch_data) {
      $operations[] = [
        '\Drupal\lang_none_default\Form\BulkUpdate::batchProcess',
        [$smaller_batch_data],
      ];
    }

    $batch = [
      'title' => t('Updating database tables...'),
      'operations' => $operations,
      'finished' => '\Drupal\lang_none_default\Form\BulkUpdate::batchFinished',
    ];

    batch_set($batch);
  }

  /**
   * Processes a subset of the full batch.
   *
   * @param array $tables
   *   Subset of full batch.
   * @param array $context
   *   Batch context.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public static function batchProcess(array $tables, array &$context) {
    $db = Database::getConnection();
    $langcode = \Drupal::languageManager()->getDefaultLanguage()->getId();

    foreach ($tables as $table) {
      try {
        // Yes, it's inefficient to check this during every loop.
        // Give how few people will ever use this, and that a site will likely
        // only run this code one time, I'm leaving it as-is.
        if ($table == 'redirect') {
          $query = $db->update($table)->fields(['language' => $langcode]);
          $query->condition('language', 'und');
          $query->execute();
        }
        else {
          $query = $db->update($table)->fields(['langcode' => $langcode]);
          $query->condition('langcode', 'und');
          $query->execute();
        }
      }
      catch (\Exception $e) {
        // Nothing to do. Column likely doesn't exist.
      }
    }

    if (count($tables) < 10) {
      $processed = ((count($context['results']) - 1) * 10) + count($tables) + 10;
    }
    else {
      $processed = count($context['results']) * 10 + 10;
    }

    $context['message'] = t('Updated @items database tables.', [
      '@items' => $processed,
    ]);
    $context['results'][] = count($tables);
  }

  /**
   * Handles completion of batch operations.
   *
   * @param bool $success
   *   TRUE if the batch operation was successful.
   * @param array $results
   *   Array of results.
   * @param array $operations
   *   Operations.
   */
  public static function batchFinished($success, array $results, array $operations) {
    if ($success) {
      \Drupal::messenger()->addMessage(t('Finished successfully. CLEAR YOUR CACHES!'), 'status');
    }
    else {
      \Drupal::messenger()->addMessage(t('Finished with an error. CLEAR YOUR CACHES!'), 'error');
    }
  }

}
