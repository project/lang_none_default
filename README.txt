CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Usage
  * Maintainers


INTRODUCTION
------------

  DISCLAIMER: This is a brute force tool. Use with caution. I have used this
  successfully on a D8 site with 591 database tables, hundreds of fields across
  content types, paragraph bundles, blocks, and more.

  This module provides a bulk update tool that updates the langcode column value
  of all database records (in all tables) that have a langcode of 'und'
  (LANGUAGE_NONE). These rows will get a new langcode that matches the site's
  default (e.g., "en").

  This should only be used on a single-language site.

  I created this module after discovering many of my migrated (d7 to d8) nodes
  became uneditable (throwing PHP errors) due to their language being 'und'.
  The first iteration of the module attempted to load and resave every node
  into the site's default language. It lead to undesirable outcomes.

  This module also includes a helper function for cleaning up duplicate URL
  aliases (favoring the alias that is in the site's default language over the
  "und" alias).

  The redirect module uses a language column instead of langcode; this will
  update that as well.

  If you have a migration that is still in use, you should update it to set the
  appropriate default language for all content it creates.


INSTALLATION
------------

  Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
  for further information.

USAGE
-----

  Visit /admin/config/regional/lang-none-default to use the bulk updater.

  I strongly recommend taking a backup and thoroughly testing your site after
  executing the bulk updater.


MAINTAINERS
-----------

  Current maintainers:
    * Adam Courtemanche - agileadam - http://www.agileadam.com
